# O<sub>2</sub> : Di-atomics United

![image](https://cdn.discordapp.com/attachments/648450847054888965/649268355684892702/Web_1920_1.png)
> Basic Wireframe UI

### Overview
<p>O2 is a platform focused on creating transparency between the user and government working and infrastructure, not just from the side of the citizens but also the officials of government can tag and reply to the feedback provided by users.</p>

![logo](https://cdn.discordapp.com/attachments/648450847054888965/649294750326980618/unknown.png) 

#### Agriculture
This is one of the main concerns of this platform, we provide open-source data of government which is updated every year of groundwater level and also general soil type data. we also provide weather data from multiple sources so that farmers can prepare themselves weather is going to change. Like all other sections with this platform covers, people can also contribute data by geotagging the groundwater level.
Another side notes on a concept which we haven’t yet tested but we would like to have. It is the ability to take data from the agricultural association of India about the fertilizers and crops being produced per session. This data can help us correlate data about “might be” causes of some adulterant related diseases caused in a particular area. 

__Summary of The Problems and solution:__
- The most noteworthy problem which our platform is meant to solve is “data availability” which will reflect in all our topics that we are going to cover in all our topics.
- Data about weather, historical climate data, soil type and (preferably) composition, groundwater level, nearby seed distribution center location laboratory locations.
- A concept which is not thought out fully: Fertilizer production, consumption usage provided by an agricultural association to help correlate the data from health diseases caused by adulterants and fertilizers.

#### Roads
General map info for quality of roads, availability all based on user feedback. This is not the focus of this platform but is easy to implement and is good to have in a centralized manner. Users can log in and post the conditions of the roads with a pic that can be removed by the admin account when the work is done.


#### Health care
General nearby healthcare info, feedback about hospitals both government and private (all geotagged) with a built-in navigation system better accessibility.
**Concept: Not entirely healthcare-specific but has been mentioned in point 3 of the summary in #Agriculture.**

### Origin of Name O2?
>What does 02 mean for us to keep this as the name? At first, O2 comes as the Life-giver, but our main motive was to have something which helps us “BURNING DOWN THE BARRIER” of information. We can’t alone burn it down as of a single “O2” atom but we can contribute to the movement of accessibility by adding as many O2 atoms (user and contributed data to open source) as possible. 
<hr></hr>

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.


> Notes : <Link> passes few of the parameters which can be fetched  
> Using redux